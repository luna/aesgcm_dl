# aesgcm_dl

Program to download encrypted files using their `aesgcm://` link in the [XEP-0454: OMEMO Media sharing](mediashare)

(Waiting for the day it becomes a numbered/published XEP...)

[mediashare]: https://xmpp.org/extensions/xep-0454.html

## Building

System depedencies:

- Zig (currently master)
- mbedtls (void-packages has it, at least)

```
zig build
```

## Usage

the `aesgcm_dl` statically links mbedtls. this enables you to ship out to
SOME linux systems around. i wasn't able to statically link musl so you
could do it for MANY linux systms.

you can get a glibc-linked mbedtls-statically-linked `x86_64-linux` executable on
https://gitlab.com/luna/aesgcm_dl/-/pipelines

```
./zig-cache/bin/aesgcm_dl aesgcm://example.com:5443/some/path/file.txt#keyinfo
```

The file will appear on `./files2/file.txt`.
