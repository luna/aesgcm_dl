const std = @import("std");
pub const c = @cImport({
    @cInclude("mbedtls/net_sockets.h");
    @cInclude("mbedtls/debug.h");
    @cInclude("mbedtls/ssl.h");
    @cInclude("mbedtls/entropy.h");
    @cInclude("mbedtls/ctr_drbg.h");
    @cInclude("mbedtls/error.h");
    @cInclude("mbedtls/certs.h");
});
extern fn tls_compat_config_init() *c.mbedtls_ssl_config;
extern fn tls_compat_config_deinit(ptr: *c.mbedtls_ssl_config) void;

pub const Context = extern struct {
    server_fd: c.mbedtls_net_context,
    entropy: c.mbedtls_entropy_context,
    ctr_drbg: c.mbedtls_ctr_drbg_context,
    ssl: c.mbedtls_ssl_context,
    cacert: c.mbedtls_x509_crt,
    conf: *c.mbedtls_ssl_config,

    pub fn deinit(self: *@This()) void {
        c.mbedtls_net_free(&self.server_fd);
        c.mbedtls_x509_crt_free(&self.cacert);
        c.mbedtls_ssl_free(&self.ssl);
        c.mbedtls_ctr_drbg_free(&self.ctr_drbg);
        c.mbedtls_entropy_free(&self.entropy);

        tls_compat_config_deinit(self.conf);
    }
};

pub fn initContext(allocator: *std.mem.Allocator) !Context {
    var ctx = std.mem.zeroes(Context);
    ctx.conf = tls_compat_config_init();

    c.mbedtls_debug_set_threshold(1);

    c.mbedtls_net_init(&ctx.server_fd);
    c.mbedtls_ssl_init(&ctx.ssl);
    c.mbedtls_x509_crt_init(&ctx.cacert);
    c.mbedtls_ctr_drbg_init(&ctx.ctr_drbg);
    c.mbedtls_ssl_config_init(ctx.conf);

    c.mbedtls_entropy_init(&ctx.entropy);
    var ret = c.mbedtls_ctr_drbg_seed(&ctx.ctr_drbg, c.mbedtls_entropy_func, &ctx.entropy, null, 0);
    if (ret != 0) {
        std.debug.print("failed to seed rng. ret={d}\n", .{ret});
        return error.SeedFail;
    }

    const path = try std.cstr.addNullByte(allocator, "/etc/ssl/certs/ca-certificates.crt");
    defer allocator.free(path);

    ret = c.mbedtls_x509_crt_parse_file(&ctx.cacert, path.ptr);
    if (ret < 0) {
        std.debug.print("failed to parse ca-certificates. ret=0x{x}\n", .{ret});
        return error.CrtFail;
    }

    return ctx;
}

export fn myDebug(
    ctx: ?*anyopaque,
    level: c_int,
    file: [*c]const u8,
    line: c_int,
    str: [*c]const u8,
) callconv(.C) void {
    _ = ctx;
    _ = level;
    std.debug.print("{s}:{d}: {s}", .{ std.mem.spanZ(file), line, std.mem.spanZ(str) });
}

pub fn connect(
    allocator: *std.mem.Allocator,
    ctx: *Context,
    host: []const u8,
    port_num: u16,
) !void {
    // need two because mbedtls does smth with the memory?
    // its also the reason why we don't free it
    const host_cstr = try std.cstr.addNullByte(allocator, host);
    defer allocator.free(host_cstr);

    const port_str = try std.fmt.allocPrint(allocator, "{d}", .{port_num});
    defer allocator.free(port_str);

    const port_cstr = try std.cstr.addNullByte(allocator, port_str);
    defer allocator.free(port_cstr);

    var ret = c.mbedtls_net_connect(&ctx.server_fd, host_cstr, port_cstr, c.MBEDTLS_NET_PROTO_TCP);
    if (ret != 0) {
        std.debug.print("tls connect failed. got {d}\n", .{ret});
        return error.ConnectFail;
    }

    ret = c.mbedtls_ssl_config_defaults(
        ctx.conf,
        c.MBEDTLS_SSL_IS_CLIENT,
        c.MBEDTLS_SSL_TRANSPORT_STREAM,
        c.MBEDTLS_SSL_PRESET_DEFAULT,
    );
    if (ret != 0) {
        std.debug.print("config_defaults failed. got {d}\n", .{ret});
        return error.ConfigFail;
    }

    c.mbedtls_ssl_conf_authmode(ctx.conf, c.MBEDTLS_SSL_VERIFY_OPTIONAL);
    c.mbedtls_ssl_conf_ca_chain(ctx.conf, &ctx.cacert, null);
    c.mbedtls_ssl_conf_rng(ctx.conf, c.mbedtls_ctr_drbg_random, &ctx.ctr_drbg);
    c.mbedtls_ssl_conf_dbg(ctx.conf, myDebug, null);

    ret = c.mbedtls_ssl_setup(&ctx.ssl, ctx.conf);
    if (ret != 0) {
        std.debug.print("setup failed. got {d}\n", .{ret});
        return error.SetupFail;
    }

    ret = c.mbedtls_ssl_set_hostname(&ctx.ssl, host_cstr.ptr);
    if (ret != 0) {
        std.debug.print("set_hostname failed. got {d}\n", .{ret});
        return error.HostnameFail;
    }

    c.mbedtls_ssl_set_bio(
        &ctx.ssl,
        &ctx.server_fd,
        c.mbedtls_net_send,
        c.mbedtls_net_recv,
        null,
    );
}

pub fn handshake(ctx: *Context) !void {
    while (true) {
        const ret = c.mbedtls_ssl_handshake(&ctx.ssl);
        std.debug.print("handshake call. got {d}\n", .{ret});

        switch (ret) {
            0 => break,
            c.MBEDTLS_ERR_SSL_WANT_READ, c.MBEDTLS_ERR_SSL_WANT_WRITE => continue,
            else => {
                std.debug.print("handshake failure: ret={x}\n", .{ret});
                return error.HandshakeFail;
            },
        }
    }
}

pub fn verify(ctx: *Context) !void {
    const flags = c.mbedtls_ssl_get_verify_result(&ctx.ssl);

    if (flags != 0) {
        var buffer: [512]u8 = undefined;

        const bytes = c.mbedtls_x509_crt_verify_info(
            &buffer,
            @sizeOf(@TypeOf(buffer)),
            "",
            flags,
        );
        var msg = buffer[0..@as(usize, @intCast(bytes))];

        std.debug.print("verify failed: {s}\n", .{msg});
    }
}

pub fn write(ctx: *Context, data: []const u8) !usize {
    while (true) {
        const ret = c.mbedtls_ssl_write(&ctx.ssl, data.ptr, data.len);
        std.debug.print("ssl_write call. got {d}\n", .{ret});

        if (ret > 0) return @as(usize, @intCast(ret));

        switch (ret) {
            c.MBEDTLS_ERR_SSL_WANT_READ, c.MBEDTLS_ERR_SSL_WANT_WRITE => continue,
            else => {
                std.debug.print("write failure: ret={x}\n", .{ret});
                return error.WriteFail;
            },
        }
    }
}

pub fn read(ctx: *Context, buf: []u8) !usize {
    while (true) {
        std.debug.print("call ssl_read ptr={*} len={d}\n", .{ buf.ptr, buf.len });
        const ret = c.mbedtls_ssl_read(&ctx.ssl, buf.ptr, buf.len);
        std.debug.print("call ssl_read got {d}\n\n", .{ret});

        switch (ret) {
            c.MBEDTLS_ERR_SSL_WANT_READ, c.MBEDTLS_ERR_SSL_WANT_WRITE => continue,
            c.MBEDTLS_ERR_SSL_PEER_CLOSE_NOTIFY => return error.Closed,
            else => {},
        }

        if (ret < 0) {
            std.debug.print("read failed. got {d}\n", .{ret});
            return error.ReadFail;
        }

        if (ret == 0) {
            return error.EndOfStream;
        }

        return @as(usize, @intCast(ret));
    }
}
