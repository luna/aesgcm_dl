const std = @import("std");
const root = @import("root");
pub const c = @cImport({
    @cInclude("mbedtls/gcm.h");
    @cInclude("mbedtls/cipher.h");
});

pub const FRAGMENT_MAX_BYTES = 32 + 16;

pub fn fragmentToBytes(fragment: []const u8, output_buffer: []u8) !usize {
    // the URL fragment contains both the key and the iv. key is 32 bytes,
    // iv is the rest (12 or 16).
    //
    // but to be able to separate key/iv, we need to parse everything to bytes
    // first. on every 2 characters we parse it as hex and write the resulting
    // byte to output_buffer
    var i: usize = 0;
    var counter: usize = 0;
    while (i < fragment.len) : (i += 2) {
        const char1 = fragment[i];
        const char2 = fragment[i + 1];

        var buffer: [2]u8 = undefined;
        var concatted = std.fmt.bufPrint(&buffer, "{c}{c}", .{ char1, char2 }) catch unreachable;

        const byte = try std.fmt.parseInt(u8, concatted, 16);
        output_buffer[counter] = byte;
        counter = counter + 1;
    }

    return counter;
}

test "fragmentToBytes" {
    const test_data = [_][]const u8{ "abcdefab", "1a2b3c" };
    const test_answers = [_][]const u8{
        &[_]u8{ 0xab, 0xcd, 0xef, 0xab },
        &[_]u8{
            0x1a,
            0x2b,
            0x3c,
        },
    };

    for (test_data, 0..) |data, idx| {
        const answer = test_answers[idx];
        const result_raw = try fragmentToBytes(data);
        const result = result_raw[0..answer.len];
        try std.testing.expectEqualSlices(u8, answer, result);
    }
}

const Result = struct { key: []const u8, iv: []const u8 };
pub fn splitBytes(bytes: []u8) Result {
    std.debug.print("fragment bytes: {} {}\n", .{ std.fmt.fmtSliceHexLower(bytes), bytes.len });

    // Fragment bytes are organized like this:
    //  - last 32 bytes is the key
    //  - anything else before that is the iv
    //
    // considering IVs are variable-sized and may be 12 or 16 bytes,
    // we may need to do some trickery
    //
    //
    // [iv...][key (32)]
    //       ^- iv_index

    const iv_index = bytes.len - 32;
    return Result{
        .key = bytes[iv_index..bytes.len],
        .iv = bytes[0..iv_index],
    };
}

pub const Context = struct {
    ctx: c.mbedtls_gcm_context,

    const Self = @This();

    pub fn init(key: []const u8, iv: []const u8) !Self {
        var self = Self{ .ctx = undefined };
        c.mbedtls_gcm_init(&self.ctx);
        try self.setKey(key);
        try self.setIV(iv);
        return self;
    }

    pub fn deinit(self: *Self) void {
        c.mbedtls_gcm_free(&self.ctx);
    }

    fn setKey(self: *Self, key: []const u8) !void {
        if (c.mbedtls_gcm_setkey(
            &self.ctx,
            c.MBEDTLS_CIPHER_ID_AES,
            key.ptr,
            @as(c_uint, @intCast(key.len * 8)),
        ) != 0) {
            // TODO map errors
            return error.SetKeyFail;
        }
    }

    fn setIV(self: *Self, iv: []const u8) !void {
        if (c.mbedtls_gcm_starts(
            &self.ctx,
            c.MBEDTLS_GCM_DECRYPT,
            iv.ptr,
            iv.len,
            null,
            0,
        ) != 0) {
            return error.StartsFail;
        }
    }

    /// Given input and output chunks must be the same size.
    pub fn decryptChunk(
        self: *Self,
        input_chunk: []const u8,
        output_chunk: []u8,
    ) !void {
        try std.testing.expectEqual(input_chunk.len, output_chunk.len);

        if (c.mbedtls_gcm_update(
            &self.ctx,
            input_chunk.len,
            input_chunk.ptr,
            output_chunk.ptr,
        ) != 0) {
            return error.UpdateFail;
        }
    }

    pub fn finalize(self: *Self) ![16]u8 {
        var result: [16]u8 = undefined;
        if (c.mbedtls_gcm_finish(&self.ctx, &result, 16) != 0) return error.FinishFail;
        return result;
    }
};
