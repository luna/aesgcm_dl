const std = @import("std");
const aes = @import("aes.zig");

pub const ByteList = std.ArrayList(u8);

const File = struct {
    allocator: std.mem.Allocator,
    host: []const u8,
    port: u16,
    path: []const u8,
    path_file: []const u8,
    fragment: []const u8,

    pub fn deinit(self: @This()) void {
        self.allocator.free(self.path);
    }
};

pub fn parseUrl(allocator: std.mem.Allocator, url_str: []const u8) !File {
    // This is a very rudimentary URL parser just for aesgcm style urls.
    // It is likely a funny looking url would make this crash.
    var url_it = std.mem.split(u8, url_str, "/");
    const scheme = url_it.next().?;
    if (!std.mem.eql(u8, scheme, "aesgcm:")) @panic("expected aesgcm:// url");

    _ = url_it.next();
    const full_host = url_it.next().?;
    std.debug.print("host: {s}\n", .{full_host});

    var host_it = std.mem.split(u8, full_host, ":");
    const host = host_it.next().?;

    // aesgcm://, as far as i know, demands https.
    const port_str = host_it.next() orelse "443";
    const port = try std.fmt.parseInt(u16, port_str, 10);

    // consuming the rest of the url by using ArrayList(u8)'s Writer is actually
    // a cool thing you can do with it.
    var path_list = ByteList.init(allocator);
    var last: []const u8 = undefined;
    while (url_it.next()) |component| {
        _ = try path_list.writer().write("/");
        _ = try path_list.writer().write(component);
        last = component;
    }

    const path_full = path_list.items;
    var path_it = std.mem.split(u8, path_full, "#");
    const path = path_it.next().?;
    const fragment = path_it.next().?;

    // remove fragment off last path component
    const idx = std.mem.indexOf(u8, last, "#").?;
    const path_file = last[0..idx];

    return File{
        .allocator = allocator,
        .host = host,
        .port = port,
        .path = path,
        .path_file = path_file,
        .fragment = fragment,
    };
}

pub const Context = struct {
    allocator: *std.mem.Allocator,
    file: File,
};

/// Caller owns returned memory.
//fn createMessage(allocator: *std.mem.Allocator, file: File) ![]const u8 {
//    var list = ByteList.init(allocator);
//
//    var the_void: [1024]u8 = undefined;
//    var reader = std.io.fixedBufferStream(&the_void).reader();
//
//    var buf: [1024]u8 = undefined;
//    var client = hzzp.base.Client.create(&buf, reader, list.writer());
//    try client.writeHead("GET", file.path);
//    try client.writeHeaderValueFormat("Host", "{s}:{d}", .{ file.host, file.port });
//    try client.writeHeaderValue("User-Agent", "aesgcm_dl (https://gitlab.com/luna/aesgcm_dl)");
//    try client.writeHeaderValue("Accept", "*/*");
//    try client.writeHeaderValue("Connection", "close");
//    try client.writeHeadComplete();
//
//    return list.items;
//}

pub const FirstChunk = struct {
    total_length: ?usize,
    chunk: []const u8,
};

const ByteFifo = std.fifo.LinearFifo(u8, .Dynamic);

const CHUNK_SIZE: usize = 2 * 1024;

pub fn decryptAndWriteChunk(
    gcm_ctx: *aes.Context,
    fifo: *ByteFifo,
    output_buffer: []u8,
    file: std.fs.File,
) !void {
    std.debug.print("decrypting a chunk from fifo\n", .{});
    var encrypt_buffer: [CHUNK_SIZE]u8 = undefined;
    try std.testing.expectEqual(CHUNK_SIZE, output_buffer.len);

    _ = fifo.read(&encrypt_buffer);
    try gcm_ctx.decryptChunk(&encrypt_buffer, output_buffer);
    _ = try file.write(output_buffer);
}

pub fn main() anyerror!u8 {
    var allocator_instance = std.heap.GeneralPurposeAllocator(.{}){};
    defer {
        _ = allocator_instance.deinit();
    }
    // var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    var allocator = allocator_instance.allocator();

    var it = std.process.args();
    _ = it.skip();

    const url_str = it.next() orelse @panic("expected url");

    const url = try parseUrl(allocator, url_str);
    defer url.deinit();

    // try to extract key and iv off url.fragment
    var fragment_buffer: [aes.FRAGMENT_MAX_BYTES]u8 = undefined;
    const count = try aes.fragmentToBytes(url.fragment, &fragment_buffer);
    const fragment_bytes = fragment_buffer[0..count];
    const aes_input = aes.splitBytes(fragment_bytes);

    std.debug.print("path: {s}\n", .{url.path});

    var headers = std.http.Headers.init(allocator);
    defer headers.deinit();

    var hostvalue = try std.fmt.allocPrint(
        allocator,
        "{s}:{d}",
        .{ url.host, url.port },
    );
    defer allocator.free(hostvalue);

    try headers.append("user-agent", "aesgcm_dl (https://gitlab.com/luna/aesgcm_dl)");
    try headers.append("accept", "*/*");
    try headers.append("host", hostvalue);
    try headers.append("connection", "close");

    var client = std.http.Client{ .allocator = allocator };
    var request = try client.request(.GET, std.Uri{
        .scheme = "https",
        .host = url.host,
        .port = url.port,
        .path = url.path,
        .user = null,
        .password = null,
        .query = null,
        .fragment = null,
    }, headers, .{});

    //std.debug.print("message: {}\nstart...\n", .{request});
    try request.start();
    std.debug.print("wait...\n", .{});
    try request.wait();

    var pathbuf: [512]u8 = undefined;
    const file_path = try std.fmt.bufPrint(&pathbuf, "files2/{s}", .{url.path_file});
    try std.fs.cwd().makePath("files2");
    var file = try std.fs.cwd().createFile(file_path, .{});
    defer file.close();

    // setup our aesgcm context
    var gcm_ctx = try aes.Context.init(aes_input.key, aes_input.iv);
    defer gcm_ctx.deinit();

    // the mbedtls api requires chunk lengths to be multiples of 16.
    //
    // there may be tls reads that don't actually return the size i want,
    // most notably, the first chunk, since that's (my read - http response)
    // bytes long, which can't be guaranteed to be a multiple of 16 (of course)
    //
    // instead of operating on intermediate buffers, we create a fifo,
    // fill it up with reads, and extract 2048 bytes out of it when
    // its available.

    std.debug.print("reading first chunk\n", .{});

    var fifo = ByteFifo.init(allocator);
    defer fifo.deinit();

    //const debug_first_bytes = chunk[0..32];
    //std.debug.print("first encrypted bytes: {x} {}\n", .{ debug_first_bytes, debug_first_bytes.len });

    var output_chunk = try allocator.alloc(u8, CHUNK_SIZE);
    defer allocator.free(output_chunk);

    while (true) {
        var buffer: [CHUNK_SIZE]u8 = undefined;
        const read_loop_bytes = request.read(&buffer) catch |err| switch (err) {
            error.EndOfStream => break,
            //error.Closed => break,
            else => return err,
        };

        const incoming_data = buffer[0..read_loop_bytes];
        std.debug.print("read {} bytes from tls\n", .{read_loop_bytes});

        if (read_loop_bytes == 0) break;

        try fifo.write(incoming_data);

        // if we have enough items in the fifo, decrypt them
        while (fifo.readableLength() > CHUNK_SIZE) {
            try decryptAndWriteChunk(&gcm_ctx, &fifo, output_chunk, file);
        }
    }

    // when the connection closes, we still have bytes leftover to decrypt
    // we must drain it all. when handling the last chunk, extract the auth
    // tag.
    while (true) {
        _ = fifo.readableLength();
        if (fifo.readableLength() < CHUNK_SIZE) {
            // last chunk, its allowed to be a non-multiple-of-16 because
            // its the last call.
            var input_chunk_buffer: [CHUNK_SIZE]u8 = undefined;
            const read_fifo_bytes = fifo.read(&input_chunk_buffer);
            var full_input_chunk = input_chunk_buffer[0..read_fifo_bytes];

            // full = [proper] + [tag (16 bytes)]
            const proper_input = full_input_chunk[0..(full_input_chunk.len - 16)];
            const proper_output = output_chunk[0..proper_input.len];

            const extracted_tag = full_input_chunk[(full_input_chunk.len - 16)..full_input_chunk.len];

            //const debug_last_bytes = full_input_chunk[(full_input_chunk.len - 128)..full_input_chunk.len];
            //std.debug.print("last incoming bytes: {x} {}\n", .{ debug_last_bytes, debug_last_bytes.len });

            try gcm_ctx.decryptChunk(proper_input, proper_output);
            _ = try file.write(proper_output);

            const output_tag = try gcm_ctx.finalize();
            std.debug.print("wanted tag: {} {}\n", .{ std.fmt.fmtSliceHexLower(extracted_tag), extracted_tag.len });
            std.debug.print("finalized tag: {} {}\n", .{ std.fmt.fmtSliceHexLower(&output_tag), 16 });

            try std.testing.expectEqualSlices(u8, extracted_tag, &output_tag);

            break;
        } else {
            try decryptAndWriteChunk(&gcm_ctx, &fifo, output_chunk, file);
        }
    }

    std.debug.print("decrypted file written to {s}\n", .{file_path});
    return 0;
}

test "among" {
    std.meta.refAllDecls(aes);
}
