#include <stdlib.h>
#include <mbedtls/ssl.h>

// this is an interface file between mbedtls and zig.

// the mbedtls_ssl_config struct contains bitflags, and because of that,
// zig translate-c demotes the struct to an opaque struct.

// issue with that is that mbedtls api requires you to have full ownership
// of that struct, and examples say it should be on the stack. then pass the
// address of that struct to wanted functions.

// the first attempt was by removing the bitflags, translating to zig via
// translate-c, then adding a pad by the end to give it enough space to hold
// the bitflags. this turned out to "work", at least in the start, but
// cryptic segfaults started to happen. many hours were wasted there.

// the second attempt (this one) is based on the fact that c handles the
// struct just fine, and since its all pointers, we can allocate the full
// struct in the heap using a c compatibility layer. zig code would only be
// able to use the opaque struct pointer, which is fine. mbedtls api only uses
// that pointer.

mbedtls_ssl_config* tls_compat_config_init() {
    mbedtls_ssl_config* ptr = malloc(sizeof(mbedtls_ssl_config));
    return ptr;
}

void tls_compat_config_deinit(mbedtls_ssl_config* ptr) {
    free(ptr);
}
